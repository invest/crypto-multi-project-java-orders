package com.invest.crypto.services.order.buy;

import java.math.BigDecimal;

/**
 * @author Kiril.m
 */
class BuyOrder {

	private long id;
	private String marketName;
	private BigDecimal originalAmount;
	private BigDecimal baseAmountWithoutFees;
	private BigDecimal quantity;
	private BigDecimal rate;
	private BigDecimal fee;
	private BigDecimal clearanceFee;
	private BigDecimal currencyRate;
	private String uuid;
	private BigDecimal commission;

	BuyOrder(	long id, String marketName, BigDecimal originalAmount, BigDecimal baseAmountWithoutFees, BigDecimal fee,
				BigDecimal clearanceFee, BigDecimal currencyRate) {
		this.id = id;
		this.marketName = marketName;
		this.originalAmount = originalAmount;
		this.baseAmountWithoutFees = baseAmountWithoutFees;
		this.fee = fee;
		this.clearanceFee = clearanceFee;
		this.currencyRate = currencyRate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public BigDecimal getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(BigDecimal amount) {
		this.originalAmount = amount;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public BigDecimal getClearanceFee() {
		return clearanceFee;
	}

	public void setClearanceFee(BigDecimal clearanceFee) {
		this.clearanceFee = clearanceFee;
	}

	public BigDecimal getCurrencyRate() {
		return currencyRate;
	}

	public void setCurrencyRate(BigDecimal currencyRate) {
		this.currencyRate = currencyRate;
	}

	public BigDecimal getBaseAmountWithoutFees() {
		return baseAmountWithoutFees;
	}

	public void setBaseAmountWithoutFees(BigDecimal baseAmountWithoutFees) {
		this.baseAmountWithoutFees = baseAmountWithoutFees;
	}

	public BigDecimal getCommission() {
		return commission;
	}

	public void setCommission(BigDecimal commission) {
		this.commission = commission;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "id: " + id + ls
				+ "marketName: " + marketName + ls
				+ "originalAmount: " + originalAmount + ls
				+ "baseAmountWithoutFees: " + baseAmountWithoutFees + ls
				+ "fee: " + fee + ls
				+ "clearanceFee: " + clearanceFee + ls
				+ "quantity: " + quantity + ls
				+ "currencyRate: " + currencyRate + ls
				+ "rate: " + rate + ls
				+ "uuid: " + uuid + ls
				+ "commission: " + commission + ls;
	}
}