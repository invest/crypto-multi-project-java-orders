package com.invest.crypto.services.order.buy;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.bittrex.model.marketapi.OrderCreated;
import com.invest.crypto.bittrex.model.publicapi.OrderBookEntry;
import com.invest.crypto.bittrex.model.publicapi.Ticker;
import com.invest.crypto.common.email.EmailUtil;
import com.invest.crypto.services.order.OrderManager;

/**
 * @author Kiril.m
 */
class BuyOrderManager extends OrderManager {

	private static final Logger log = LogManager.getLogger(BuyOrderManager.class);
	static final String SKIP_BUY_LIMIT = "SKIP_BITREX_CONFIRMATION";

	static List<BuyOrder> getOrders(/* int stateId */) {
		Connection con = null;
		try {
			con = getConnection();
			return BuyOrderDAO.getOrders(con/* , stateId */);
		} catch (SQLException e) {
			log.debug("Unable to load orders in state 10", e);
			return new ArrayList<>();
		} finally {
			closeConnection(con);
		}
	}

	static boolean lockOrder(long orderId) {
		Connection con = null;
		try {
			con = getConnection();
			return BuyOrderDAO.lockOrder(con, orderId);
		} catch (SQLException e) {
			log.debug("Unable to lock order with id " + orderId, e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	static void unlockOrder(long orderId, String orderUuid, BigDecimal quantity, BigDecimal rate, BigDecimal commission, boolean skipBuyLimit) {
		Connection con = null;
		try {
			con = getConnection();
			OrderState state = OrderState.PROCESSED;
			String comments = null;
			if (skipBuyLimit) {
				state = OrderState.PROCESSING_SKIPPED;
				comments = SKIP_BUY_LIMIT + " = " + skipBuyLimit;
			}
			if (!BuyOrderDAO.unlockOrder(con, orderId, orderUuid, quantity, rate, state, comments, commission)) {
				log.warn("Unlock order update in the db did not execute as expected");
			}
		} catch (SQLException e) {
			log.debug("Unable to unlock order with id " + orderId, e);
		} finally {
			closeConnection(con);
		}
	}

	static BuyOrder placeBuyLimit(String apiKey, BuyOrder order, boolean skipBuyLimit) {
		try {
			Ticker ticker = getPublicApi().getTicker(order.getMarketName());
			BigDecimal rate = ticker.getAsk(); // .multiply(new BigDecimal("1.0025"));
			BigDecimal rateCents = rate.multiply(new BigDecimal("100"));
			BigDecimal quantity = order.getBaseAmountWithoutFees().divide(rateCents, 8, RoundingMode.HALF_UP);
			BigDecimal commission = calculateCommission(order.getMarketName());
			order.setQuantity(quantity.subtract(commission, new MathContext(6, RoundingMode.HALF_UP)));
			order.setCommission(commission);
			order.setRate(rateCents);
			if (!skipBuyLimit) {
				// rate need to be set based on orderbook call
				ArrayList<OrderBookEntry> orderBookSellList = getPublicApi().getOrderBookForSell(order.getMarketName());
				if (orderBookSellList.isEmpty()) {
					log.debug("orderBookSellList is empty, rate is set from ticker call.......");
				} else {
					BigDecimal quantityTmp = new BigDecimal("0");
					BigDecimal one = new BigDecimal("1");
					for (OrderBookEntry obe : orderBookSellList) {
						quantityTmp = quantityTmp.add(obe.getQuantity());
						if (quantity.add(one).compareTo(quantityTmp) < 0) {
							rate = obe.getRate();
							order.setRate(rate.multiply(new BigDecimal("100")));
							log.debug("order rate is set based on orderBookSellList accumulated value, which is: " + obe.getRate());
							break;
						}
					}
				}

				// TODO apiKey should be used
				OrderCreated orderCreated = getMarketApi().buyLimit(/* apiKey, */order.getMarketName(), quantity, rate);
				if (orderCreated != null) {
					order.setUuid(orderCreated.getUuid());
				}
			}
			return order;
		} catch (IOException e) {
			log.debug("Unable to place buy limit order. Returning null", e);
			EmailUtil.sendAlertEmail(	"com.invest.crypto.services.order.buy.BuyOrderManager.placeBuyLimit(String, BuyOrder) exception",
										"Unable to place buy limit order.\n" + e);
			return null;
		} catch (InvalidKeyException | NoSuchAlgorithmException | IllegalStateException | SQLException e) {
			log.debug("Unable to make buy limit request. Returning null", e);
			EmailUtil.sendAlertEmail(	"com.invest.crypto.services.order.buy.BuyOrderManager.placeBuyLimit(String, BuyOrder) exception",
										"Unable to make buy limit request.\n" + e);
			return null;
		}
	}

	static void updateFailedOrder(long orderId, BigDecimal quantity, BigDecimal rate) {
		Connection con = null;
		try {
			con = getConnection();
			if (!BuyOrderDAO.updateFailedOrder(con, orderId, quantity, rate)) {
				log.warn("Update of failed order did not execute as expected");
			}
		} catch (SQLException e) {
			log.debug("Unable to update failed order with id " + orderId, e);
		} finally {
			closeConnection(con);
		}
	}

	static boolean isBuyLimitSkipped() {
		Connection con = null;
		try {
			con = getConnection();
			return BuyOrderDAO.isBuyLimitSkipped(con);
		} catch (SQLException e) {
			log.error("Unable to determine " + SKIP_BUY_LIMIT, e);
			StackTraceElement[] st = Thread.currentThread().getStackTrace();
			String ls = System.lineSeparator();
			StringBuffer sb = new StringBuffer();
			sb.append(ls).append("Stack trace:").append(ls);
			for (int i = 0; i < st.length; i++) {
				sb	.append(st[i].getClassName()).append(".").append(st[i].getMethodName()).append("(").append(st[i].getFileName())
					.append(":").append(st[i].getLineNumber()).append(")").append(ls);
			}
			throw new InvalidParameterException(sb.toString());
		} finally {
			closeConnection(con);
		}
	}
	
	private static BigDecimal calculateCommission(String marketName) {
		BigDecimal commission = new BigDecimal("0");
		try {
			Map<String, BigDecimal> commissionList = BuyOrderManager.getBittrexCommissions();
			String[] pairs = marketName.split("-");
			if (!commissionList.isEmpty() && commissionList.containsKey(pairs[1])) {
				commission = commissionList.get(pairs[1]);
			} else {
				log.debug("Bittrex commission can not be retrieved from DB, so 0 (zero) is set insted. ");
			}
		} catch (Exception e) {
			log.debug("Bittrex commission can not be retreived, NULL is inserted!");
		}
		return commission;
	}

	private static Map<String, BigDecimal> getBittrexCommissions() {
		Connection con = null;
		try {
			con = getConnection();
			return BuyOrderDAO.getBittrexCommissions(con);
		} catch (SQLException e) {
			log.debug("Unable to get bittrex commissions due to: ", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}
}