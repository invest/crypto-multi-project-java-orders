package com.invest.crypto.services.order.buy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Kiril.m
 */
class BuyOrderTask implements Runnable {

	private static final Logger log = LogManager.getLogger(BuyOrderTask.class);
	private String apiKey;
	private BuyOrder buyOrder;

	public BuyOrderTask(String apiKey, BuyOrder buyOrder) {
		this.apiKey = apiKey;
		this.buyOrder = buyOrder;
	}

	@Override
	public void run() {
		log.debug("Starting buy order task for " + buyOrder);
		if (BuyOrderManager.lockOrder(buyOrder.getId())) {
			boolean skipBuyLimit = BuyOrderManager.isBuyLimitSkipped();
			BuyOrder createdOrder = BuyOrderManager.placeBuyLimit(apiKey, buyOrder, skipBuyLimit);
			if (createdOrder != null && (createdOrder.getUuid() != null || skipBuyLimit)) {
				BuyOrderManager.unlockOrder(buyOrder.getId(), createdOrder.getUuid(), createdOrder.getQuantity(), createdOrder.getRate(),
											createdOrder.getCommission(), skipBuyLimit);
			} else {
				BuyOrderManager.updateFailedOrder(buyOrder.getId(), buyOrder.getQuantity(), buyOrder.getRate());
			}
		} else {
			log.debug("The order could not be locked. Probably it's already locked");
		}
		log.debug("Task completed for buy order with id " + buyOrder.getId());
	}
}