package com.invest.crypto.services.order.check;

import java.math.BigDecimal;

import com.invest.crypto.services.order.OrderManager.OrderState;

/**
 * @author Kiril.m
 */
class CheckOrder {

	private long id;
	private String uuid;
	private String toCurrency;
	private BigDecimal quantity;
	private String wallet;
	private long transactionId;
	private OrderState state;
	private BigDecimal commission;

	public CheckOrder(long id, String uuid, String toCurrency, BigDecimal quantity, String wallet, long transactionId, OrderState state, BigDecimal commission) {
		this.id = id;
		this.uuid = uuid;
		this.toCurrency = toCurrency;
		this.quantity = quantity;
		this.wallet = wallet;
		this.transactionId = transactionId;
		this.state = state;
		this.commission = commission;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getToCurrency() {
		return toCurrency;
	}

	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getWallet() {
		return wallet;
	}

	public void setWallet(String wallet) {
		this.wallet = wallet;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public OrderState getState() {
		return state;
	}

	public void setState(OrderState state) {
		this.state = state;
	}

	public BigDecimal getCommission() {
		return commission;
	}

	public void setCommission(BigDecimal commission) {
		this.commission = commission;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "id: " + id + ls
				+ "uuid: " + uuid + ls
				+ "toCurrency: " + toCurrency + ls
				+ "quantity: " + quantity + ls
				+ "wallet: " + wallet + ls
				+ "transactionId: " + transactionId + ls
				+ "state: " + state + ls
				+ "commission: " + commission + ls;
	}
}