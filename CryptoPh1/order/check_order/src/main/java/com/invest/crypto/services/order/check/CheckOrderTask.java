package com.invest.crypto.services.order.check;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.bittrex.model.accountapi.Order;

/**
 * @author Kiril.m
 */
class CheckOrderTask implements Runnable {

	private static final Logger log = LogManager.getLogger(CheckOrderTask.class);
	private CheckOrder checkOrder;

	public CheckOrderTask(CheckOrder checkOrder) {
		this.checkOrder = checkOrder;
	}

	@Override
	public void run() {
		log.debug("Starting check order task for " + checkOrder);
		Order order = CheckOrderManager.checkOrder(checkOrder.getUuid(), checkOrder.getState());
		if (order != null && order.getIsOpen() != null && !order.getIsOpen()) {
			if (CheckOrderManager.closeOrder(checkOrder.getId())) {
				CheckOrderManager.insertWithdraw(checkOrder);
			} else {
				log.warn("Order with id " + checkOrder.getId() + " was not closed. Withdraw was not inserted");
			}
		}
		log.debug("Task completed for check order with id " + checkOrder.getId());
	}
}