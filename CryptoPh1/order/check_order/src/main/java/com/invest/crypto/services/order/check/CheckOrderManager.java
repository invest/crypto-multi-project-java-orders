package com.invest.crypto.services.order.check;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.bittrex.model.accountapi.Order;
import com.invest.crypto.services.order.OrderManager;

/**
 * @author Kiril.m
 */
class CheckOrderManager extends OrderManager {

	private static final Logger log = LogManager.getLogger(CheckOrderManager.class);
	static final String WITHDRAW_MANUAL_MODE = "MANUAL_MODE";

	static List<CheckOrder> getOrders(/* int stateId */) {
		Connection con = null;
		try {
			con = getConnection();
			return CheckOrderDAO.getOrders(con/* , stateId */);
		} catch (SQLException e) {
			log.debug("Unable to load orders in state 30", e);
			return new ArrayList<>();
		} finally {
			closeConnection(con);
		}
	}

	static Order checkOrder(String uuid, OrderState orderState) {
		try {
			switch (orderState) {
			case PROCESSING_SKIPPED:
				Order order = new Order();
				order.setIsOpen(false);
				return order;

			case PROCESSED:
				// TODO apiKey should be used
				return getAccountApi().getOrder(uuid);

			default:
				return null;
			}
		} catch (IOException e) {
			log.debug("Unable to check order. Returning null", e);
			return null;
		} catch (InvalidKeyException | NoSuchAlgorithmException | IllegalStateException | SQLException e) {
			log.debug("Unable to make check order request. Returning null", e);
			return null;
		}
	}

	static boolean closeOrder(long orderId) {
		Connection con = null;
		try {
			con = getConnection();
			return CheckOrderDAO.closeOrder(con, orderId);
		} catch (SQLException e) {
			log.debug("Unable to close order with id " + orderId, e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	static void insertWithdraw(CheckOrder checkOrder) {
		Connection con = null;
		try {
			con = getConnection();
			OrderState state;
			if (CheckOrderDAO.isManualWithdrawMode(con)) {
				state = OrderState.READY_FOR_MANUAL_PROCESSING;
			} else {
				state = OrderState.READY_FOR_PROCESSING;
			}
			CheckOrderDAO.insertWithdraw(con, checkOrder, state);
		} catch (SQLException e) {
			log.debug("Unable to insert withdraw for order with id " + checkOrder.getId(), e);
		} finally {
			closeConnection(con);
		}
	}
}