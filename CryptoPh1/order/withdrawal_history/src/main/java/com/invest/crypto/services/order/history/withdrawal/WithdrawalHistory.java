package com.invest.crypto.services.order.history.withdrawal;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.invest.crypto.bittrex.model.accountapi.WithdrawalHistoryEntry;

/**
 * @author Kiril.m
 */
class WithdrawalHistory extends WithdrawalHistoryEntry {

	private final String paymentUuid;
	private BigDecimal amountBd;
	private volatile int hash;

	public WithdrawalHistory(WithdrawalHistoryEntry historyEntry) {
		paymentUuid = historyEntry.getPaymentUuid();
		setPaymentUuid(paymentUuid);
		setCurrency(historyEntry.getCurrency());
		setAmount(historyEntry.getAmount());
		if (getAmount() != null) {
			setAmountBd(new BigDecimal(getAmount()));
		}
		setAddress(historyEntry.getAddress());
		setOpened(historyEntry.getOpened());
		setAuthorized(historyEntry.getAuthorized());
		setPendingPayment(historyEntry.getPendingPayment());
		setTxCost(historyEntry.getTxCost());
		setTxId(historyEntry.getTxId());
		setCanceled(historyEntry.getCanceled());
		setInvalidAddress(historyEntry.getInvalidAddress());
	}

	public WithdrawalHistory(	String paymentUuid, String currency, BigDecimal amount, String address, LocalDateTime opened, Boolean authorized,
								Boolean pendingPayment, Double txCost, String txId, Boolean canceled, Boolean invalidAddress) {
		this.paymentUuid = paymentUuid;
		setPaymentUuid(paymentUuid);
		setCurrency(currency);
		setAmountBd(amount);
		setAmount(amountBd.toString());
		setAddress(address);
		setOpened(opened);
		setAuthorized(authorized);
		setPendingPayment(pendingPayment);
		setTxCost(txCost);
		setTxId(txId);
		setCanceled(canceled);
		setInvalidAddress(invalidAddress);
	}

	public String getPaymentUuid() {
		return paymentUuid;
	}

	public BigDecimal getAmountBd() {
		return amountBd;
	}

	public void setAmountBd(BigDecimal amountBd) {
		this.amountBd = amountBd;
	}

	@Override
	public int hashCode() {
		int h = hash;
		if (h == 0) {
			final int prime = 31;
			h = 1;
			h = prime * h + ((paymentUuid == null) ? 0 : paymentUuid.hashCode());
			hash = h;
		}
		return h;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		WithdrawalHistory other = (WithdrawalHistory) obj;
		if (paymentUuid == null) {
			if (other.getPaymentUuid() != null) {
				return false;
			}
		} else if (!paymentUuid.equals(other.getPaymentUuid())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "paymentUuid: " + getPaymentUuid() + ls
				+ super.getClass().getName() + ".paymentUuid: " + super.getPaymentUuid() + ls
				+ "currency: " + getCurrency() + ls
				+ "amount: " + getAmount() + ls
				+ "address: " + getAddress() + ls
				+ "opened: " + getOpened() + ls
				+ "authorized: " + getAuthorized() + ls
				+ "pendingPayment: " + getPendingPayment() + ls
				+ "txCost: " + getTxCost() + ls
				+ "txId: " + getTxId() + ls
				+ "canceled: " + getCanceled() + ls
				+ "invalidAddress: " + getInvalidAddress() + ls;
	}
}