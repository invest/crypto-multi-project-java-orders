package com.invest.crypto.services.order.history.withdrawal;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.services.order.OrderConfig;
import com.invest.crypto.services.order.OrderService;
import com.invest.crypto.services.order.history.withdrawal.WithdrawalHistoryManager.WithdrawalHistoryCurrency;

/**
 * @author Kiril.m
 */
class WithdrawalHistoryService extends OrderService {

	private static final Logger log = LogManager.getLogger(WithdrawalHistoryService.class);
	private String apiKey;

	@Override
	public void initService() {
		OrderConfig config = WithdrawalHistoryManager.getOrderConfig();
		if (config != null) {
			apiKey = config.getApiKey();
			executor = new WithdrawalHistoryExecutor(1);
			scheduledPeriod = config.getScheduledPeriod();
		} else {
			log.warn("Executor is null. Scheduling of task will fail");
		}

	}

	@Override
	protected Logger getLogger() {
		return log;
	}

	private class WithdrawalHistoryExecutor implements Runnable {

		private ExecutorService taskExecutor;

		public WithdrawalHistoryExecutor(int taskCount) {
			taskExecutor = Executors.newFixedThreadPool(taskCount);
		}

		@Override
		public void run() {
			log.debug("WithdrawalHistoryExecutor has started");
			List<Future<?>> futures = new ArrayList<>();
			for (WithdrawalHistoryCurrency currency : WithdrawalHistoryCurrency.values()) {
				futures.add(taskExecutor.submit(new WithdrawalHistoryTask(apiKey, currency)));
			}
			for (Future<?> future : futures) {
				try {
					future.get(); // in order to wait all tasks
				} catch (InterruptedException e) {
					log.debug("Thread was interrupted", e);
					// Preserve interrupt status
					Thread.currentThread().interrupt();
				} catch (ExecutionException e) {
					log.debug("Unable to retrieve result", e);
				}
			}
			log.debug("WithdrawalHistoryExecutor has completed");
		}
	}
}