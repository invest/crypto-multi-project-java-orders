package com.invest.crypto.services.order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.invest.crypto.common.util.DAOBase;
import com.invest.crypto.services.order.OrderManager.OrderConfigKey;

/**
 * @author Kiril.m
 */
public abstract class OrderDAO extends DAOBase {

	static OrderConfig getOrderConfig(Connection con) throws SQLException {
		String sql = "select config_key, config_value from crypto_config where config_key in(?, ?, ?)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, OrderConfigKey.API_KEY.toString());
			ps.setString(2, OrderConfigKey.SCHEDULED_PERIOD.toString());
			ps.setString(3, OrderConfigKey.TASK_COUNT.toString());
			ResultSet rs = ps.executeQuery();
			String apiKey = null;
			long scheduledPeriod = 0;
			int taskCount = 0;
			while (rs.next()) {
				OrderConfigKey configKey = OrderConfigKey.valueOf(rs.getString("config_key"));
				switch (configKey) {
				case API_KEY:
					apiKey = rs.getString("config_value");
					break;

				case SCHEDULED_PERIOD:
					scheduledPeriod = rs.getLong("config_value");
					break;

				case TASK_COUNT:
					taskCount = rs.getInt("config_value");
					break;

				default:
					break;
				}
			}
			if (apiKey != null && scheduledPeriod > 0 && taskCount > 0) {
				return new OrderConfig(apiKey, scheduledPeriod, taskCount);
			}
			return null;
		}
	}
}