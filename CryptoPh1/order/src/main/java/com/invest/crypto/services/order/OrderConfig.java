package com.invest.crypto.services.order;

/**
 * @author Kiril.m
 */
public class OrderConfig {

	private String apiKey;
	private long scheduledPeriod;
	private int taskCount;

	public OrderConfig(String apiKey, long scheduledPeriod, int taskCount) {
		this.apiKey = apiKey;
		this.scheduledPeriod = scheduledPeriod;
		this.taskCount = taskCount;
	}

	public OrderConfig(String apiKey, long scheduledPeriod) {
		this.apiKey = apiKey;
		this.scheduledPeriod = scheduledPeriod;
	}

	public String getApiKey() {
		return apiKey;
	}

	public long getScheduledPeriod() {
		return scheduledPeriod;
	}

	public int getTaskCount() {
		return taskCount;
	}
}