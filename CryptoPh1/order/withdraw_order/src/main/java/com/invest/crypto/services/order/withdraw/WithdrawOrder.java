package com.invest.crypto.services.order.withdraw;

import java.math.BigDecimal;

/**
 * @author Kiril.m
 */
class WithdrawOrder {

	private long id;
	private String currency;
	private BigDecimal quantity;
	private String wallet;
	private long transactionId;

	public WithdrawOrder(long id, String currency, BigDecimal quantity, String wallet, long transactionId) {
		this.id = id;
		this.currency = currency;
		this.quantity = quantity;
		this.wallet = wallet;
		this.transactionId = transactionId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getWallet() {
		return wallet;
	}

	public void setWallet(String wallet) {
		this.wallet = wallet;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "id: " + id + ls
				+ "currency: " + currency + ls
				+ "quantity: " + quantity + ls
				+ "wallet: " + wallet + ls
				+ "transactionId: " + transactionId + ls;
	}
}