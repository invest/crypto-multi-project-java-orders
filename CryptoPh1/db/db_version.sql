-------------------------------------------------------------------------------------------------------------------------------------------
-- change-db
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE CRYPTO_CONFIG 
(
  CONFIG_KEY VARCHAR2(64) NOT NULL 
, CONFIG_VALUE VARCHAR2(255) NOT NULL 
, CONSTRAINT CRYPTO_CONFIG_PK PRIMARY KEY 
  (
    CONFIG_KEY 
  )
  ENABLE 
);

CREATE TABLE CRYPTO_CONFIG_TEST_API 
(
  ID NUMBER NOT NULL 
, SERVICE_NAME VARCHAR2(20) NOT NULL 
, TEST_KEY VARCHAR2(20) NOT NULL 
, TEST_VALUE VARCHAR2(200) NOT NULL 
, DT_CREATED DATE DEFAULT sysdate NOT NULL 
, CONSTRAINT CRYPTO_CONFIG_TEST_API_PK PRIMARY KEY 
  (
    ID 
  )
  ENABLE 
);

CREATE PUBLIC SYNONYM CRYPTO_CONFIG FOR ETRADER.CRYPTO_CONFIG;
GRANT SELECT, INSERT, UPDATE ON CRYPTO_CONFIG TO ETRADER_WEB_CONNECT;

CREATE PUBLIC SYNONYM CRYPTO_CONFIG_TEST_API FOR ETRADER.CRYPTO_CONFIG_TEST_API;
GRANT SELECT, INSERT, UPDATE ON CRYPTO_CONFIG_TEST_API TO ETRADER_WEB_CONNECT;

CREATE SEQUENCE SEQ_CRYPTO_WITHDRAW;
CREATE PUBLIC SYNONYM SEQ_CRYPTO_WITHDRAW FOR SEQ_CRYPTO_WITHDRAW;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------