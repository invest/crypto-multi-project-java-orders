package com.invest.crypto.common.email;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.common.util.ManagerBase;

/**
 * @author Kiril.m
 */
class EmailManager extends ManagerBase {

	private static final Logger log = LogManager.getLogger(EmailManager.class);
	
	enum EmailParams {
		SMTP_HOST, RECIPIENTS;
	}
	
	static EmailConfig getConfig() {
		Connection con = null;
		try {
			con = getConnection();
			return EmailDAO.getConfig(con);
		} catch (SQLException e) {
			log.debug("Unable to load configuration", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}
}