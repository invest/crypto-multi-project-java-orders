package com.invest.crypto.common.security;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.common.util.ManagerBase;

/**
 * @author Kiril.m
 */
class SignatureManager extends ManagerBase {

	private static final Logger log = LogManager.getLogger(SignatureManager.class);

	static byte[] getApiSecret() {
		Connection con = null;
		try {
			con = getConnection();
			return SignatureDAO.getApiSecret(con);
		} catch (SQLException e) {
			log.debug("Unable to load", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}
}