package com.invest.crypto.common.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Kiril.m
 */
public abstract class DAOBase {

	private static final Logger log = LogManager.getLogger(DAOBase.class);

	public static long getSequenceNextVal(Connection con, String seq) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT " + seq + ".nextval from dual";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong(1);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;
	}

	public static void closeStatement(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException ex) {
				log.error(stmt, ex);
			}
		}
	}

	public static void closeResultSet(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException ex) {
				log.error(rs, ex);
			}
		}
	}
}