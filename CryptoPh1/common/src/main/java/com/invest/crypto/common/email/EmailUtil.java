package com.invest.crypto.common.email;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Kiril.m
 */
public class EmailUtil {

	private static final Logger log = LogManager.getLogger(EmailUtil.class);
	private static EmailUtil util;
	private static Properties properties;
	private static InternetAddress from;
	private static InternetAddress[] to;
	private static long lastReload;
	private static long reloadInterval = 3600 * 1000;

	private static EmailUtil getInstance() throws AddressException {
		if (util == null) {
			util = new EmailUtil();
		}
		if (System.currentTimeMillis() > lastReload + reloadInterval) { // reload
			configureUtil();
		}
		return util;
	}

	private static void configureUtil() throws AddressException {
		EmailConfig config = EmailManager.getConfig();
		if (config == null) {
			log.debug("Configuration was not loaded");
			throw new IllegalArgumentException("Configuration was not loaded");
		}
		if (properties == null) {
			properties = new Properties();
		} else {
			properties.clear();
		}
		properties.put("mail.smtp.host", config.getSMTPHost());
		try {
			from = new InternetAddress("cryptoAlerts@invest.com");
			String[] toStr = config.getRecipients().split(",");
			to = new InternetAddress[toStr.length];
			int i = 0;
			for (String item : toStr) {
				to[i++] = new InternetAddress(item.trim());
			}
		} catch (AddressException e) {
			log.debug("Unable to create internet address", e);
			throw e;
		}
	}

	public static void sendAlertEmail(String subject, String body) {
		try {
			new Thread(getInstance().new EmailSender(subject, body)).start();
		} catch (Exception e) {
			log.debug("Unable to send alert email", e);
		}
	}

	private class EmailSender implements Runnable {

		private String subject;
		private String body;

		public EmailSender(String subject, String body) {
			this.subject = subject;
			this.body = body;
		}

		@Override
		public void run() {
			Session session = Session.getInstance(properties);
			MimeMessage message = new MimeMessage(session);
			try {
				message.setFrom(from);
				message.setRecipients(Message.RecipientType.TO, to);
				message.setHeader("Content-Type", "text/html; charset=UTF-8");
				message.setHeader("Content-Transfer-Encoding", "base64");
				message.setSubject(subject, "UTF-8");
				body += "\n Server name: " + determineHost();
				message.setContent(body, "text/html; charset=UTF-8");
				Transport.send(message);
			} catch (MessagingException e) {
				log.debug("Unable to send email", e);
			}
		}

		private String determineHost() {
			Map<String, String> env = System.getenv();
			if (env.containsKey("COMPUTERNAME")) {
				return env.get("COMPUTERNAME");
			} else if (env.containsKey("HOSTNAME")) {
				return env.get("HOSTNAME");
			}
			try {
				return InetAddress.getLocalHost().getHostName();
			} catch (UnknownHostException e) {
				log.debug("Unable to determine host", e);
				return null;
			}
		}
	}
}