package com.invest.crypto.common.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Kiril.m
 */
public abstract class ManagerBase {

	private static final Logger log = LogManager.getLogger(ManagerBase.class);
	private static DataSource dataSource;

    /**
     * @return The <code>DataSource</code> to obtain DB connection from. Create it if not created.
     * @throws SQLException
     */
	protected static DataSource getDataSource() {
		if (dataSource == null) {
			try {
				dataSource = (DataSource) new InitialContext().lookup("java:jboss/datasources/anyoptionDS");
			} catch (NamingException e) {
				log.debug("Unable to lookup data source", e);
			}
        }
		return dataSource;
	}

	protected static final Connection getConnection() throws SQLException {
		Connection con = null;
		con = getDataSource().getConnection();
		if (con == null) {
			log.error("GOT NULL in Connection");
		} else {
            try {
                if (!con.getAutoCommit()) {
                    log.error("************************************* ");
                    log.error("Got conn with AutoCommit false!!!!!!! ");
                    log.error("************************************* ");
                    con.setAutoCommit(true);
                }
            } catch (SQLException sqle) {
                log.warn("Can't check auto commit.", sqle);
            }
        }
		return con;
	}

	/**
	 * Close db connection.
	 * 
	 * @param conn the db conn to close
	 */
	protected static final void closeConnection(Connection con) {
		if (null != con) {
			try {
				if (!con.getAutoCommit()) {
					log.error("Return connection with autocommit false");
					StackTraceElement[] st = Thread.currentThread().getStackTrace();
					String ls = System.lineSeparator();
					StringBuffer sb = new StringBuffer();
					sb.append(ls).append("Stack trace:").append(ls);
					for (int i = 0; i < st.length; i++) {
						sb	.append(st[i].getClassName()).append(".").append(st[i].getMethodName()).append("(").append(st[i].getFileName())
							.append(":").append(st[i].getLineNumber()).append(")").append(ls);
					}
					log.error(sb.toString());
					con.setAutoCommit(true);
				}
			} catch (SQLException sqle) {
				log.warn("Can't check auto commit.", sqle);
			}
			try {
				con.close();
			} catch (SQLException sqle) {
				log.error("Can't close connection.", sqle);
			}
		}
	}

	protected static final void rollback(Connection conn) {
	    try {
	        conn.rollback();
	    } catch (SQLException sqle) {
	        log.error("Can't rollback.", sqle);
	    }
	}
	
	protected static final void setAutoCommit(Connection con, boolean autoCommit) {
        if (null != con) {
            try {
                con.setAutoCommit(autoCommit);
            } catch (SQLException sqle) {
                log.error("Can't set autocommit to " + autoCommit, sqle);
            }
        }
	}
}