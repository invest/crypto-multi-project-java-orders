package com.invest.crypto.common.security;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.invest.crypto.common.util.DAOBase;

/**
 * @author Kiril.m
 */
class SignatureDAO extends DAOBase {

	static byte[] getApiSecret(Connection con) throws SQLException {
		String sql = "select config_key, config_value from crypto_config where config_key = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, new String("API_SECRET"));
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getBytes("config_value");
			}
		}
		return null;
	}
}