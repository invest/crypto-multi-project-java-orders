/**
 * 
 */
package com.invest.crypto.bittrex.api;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.invest.crypto.bittrex.config.EnvironmentConfigManager;
//import com.invest.crypto.bittrex.config.TickerManager;
import com.invest.crypto.bittrex.model.publicapi.OrderBookEntry;
import com.invest.crypto.bittrex.model.publicapi.Ticker;
import com.invest.crypto.bittrex.util.Util;

/**
 * @author Tabakov
 *
 */
@Path("/api")
public class BittrexPublicAPI {
	private static final Logger log = LogManager.getLogger(BittrexPublicAPI.class);
	private static final String BASE_URL = "https://bittrex.com/api/v1.1/public";
	
	private static Map<String, Ticker> hashTicker = new HashMap<>();
	private static Map<LocalDateTime, Long> timeToWait = new HashMap<>();
	private static final long ONE_HOUR = 1;

	@GET
	@Path("/ticker1")
	@Produces(MediaType.APPLICATION_JSON)
	public Ticker getTicker(@QueryParam("market") String market) throws JsonProcessingException, IOException {

		long serviceCallTime = System.currentTimeMillis();

		log.debug("getticker is called with params --> market: " + market + " at: " + serviceCallTime);
		long hashInterval = 30000;// default

		// start db configuration cashe --> refresh every one hour
		if (timeToWait.isEmpty()) {
			hashInterval = EnvironmentConfigManager.getTickerHashInterval();
			timeToWait.put(LocalDateTime.now(), hashInterval);
			log.debug("First load from DB with interval : " +  hashInterval);
		} else {
			Iterator<Map.Entry<LocalDateTime, Long>> entries = timeToWait.entrySet().iterator();
			if (entries.hasNext()) {
				Map.Entry<LocalDateTime, Long> entry = entries.next();
				if (entry.getKey().plusHours(ONE_HOUR).isAfter(LocalDateTime.now())) {
					hashInterval = entry.getValue();
				} else { // one hour is passed
					timeToWait.clear();
					hashInterval = EnvironmentConfigManager.getTickerHashInterval();
					timeToWait.put(LocalDateTime.now(), hashInterval);
					log.debug("One hour is passed! Load from DB with interval : " +  hashInterval);
				}
			}
		}

		if (hashTicker.containsKey(market) && (hashTicker.get(market).getTimeCalled() + hashInterval) > serviceCallTime) {

			return hashTicker.get(market);

		} else {

			String url = BASE_URL + "/getticker?market=" + market;

			String jsonResponse = Util.getResponseRestEasy(url);

			log.debug("Bittrex response for getTicker: " + jsonResponse);

			Ticker ticker = new Ticker();
			ticker.setRequestURL(url);
			ticker.setResponse(jsonResponse);

			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode marketsResponse = objectMapper.readTree(jsonResponse);
			String success = marketsResponse.get("success").asText();
			if (success.equals("true")) {
				JsonNode result = marketsResponse.get("result");
				if (!result.isNull()) {
					ticker.setBid(result.get("Bid").isNull() ? null : result.get("Bid").decimalValue());
					ticker.setAsk(result.get("Ask").isNull() ? null : result.get("Ask").decimalValue());
					ticker.setLast(result.get("Last").isNull() ? null : result.get("Last").decimalValue());
					ticker.setTimeCalled(serviceCallTime);
					hashTicker.put(market, ticker);
				}
			}
			return ticker;
		}
	}
	
//	@GET
//	@Path("/getorderbook")//tested
//	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<OrderBookEntry> getOrderBookForSell(String market) throws JsonProcessingException, IOException {
		
		log.info("getorderbook is called with params --> market: " + market);

		String url = BASE_URL + "/getorderbook?market=" + market + "&type=sell";

		String jsonResponse = Util.getResponseRestEasy(url);
		
		OrderBookEntry orderHistoryResponse = new OrderBookEntry();
		orderHistoryResponse.setResponse(jsonResponse);
		orderHistoryResponse.setRequestURL(url);
		
		//too big to white on server log
		//log.info("Bittrex response for getOrderBookForSell: " + jsonResponse);

		ObjectMapper objectMapper = new ObjectMapper();
		ArrayList<OrderBookEntry> orderBookEntryList = new ArrayList<OrderBookEntry>();
//		orderBookEntryList.add(orderHistoryResponse);

		JsonNode orderResponse = objectMapper.readTree(jsonResponse);
		String success = orderResponse.get("success").asText();
		if (success.equals("true")) {
			JsonNode result = orderResponse.get("result");
			if(!result.isNull()) {
				if (result.isArray()) {
					for (JsonNode o : result) {
						OrderBookEntry orderBookEntry = new OrderBookEntry();
						orderBookEntry.setQuantity(o.get("Quantity").decimalValue());
						orderBookEntry.setRate(o.get("Rate").decimalValue());
						orderBookEntryList.add(orderBookEntry);
					}
				}
			}

		}
		return orderBookEntryList;
	}
}
