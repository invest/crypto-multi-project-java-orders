package com.invest.crypto.bittrex.model.marketapi;

import com.invest.crypto.bittrex.model.common.Response;

/**
 * @author Tabakov
 */
public class OrderCreated extends Response {

	private String uuid;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		return "OrderCreated [uuid=" + uuid + ", getUuid()=" + getUuid() + ", getResponse()=" + getResponse()
				+ ", getRequestURL()=" + getRequestURL() + ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}
}
