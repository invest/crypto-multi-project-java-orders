package com.invest.crypto.bittrex.model.common;

/**
 * @author Tabakov
 */
public class BittrexApiException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7554935198037971186L;

	public BittrexApiException() {
    }

    public BittrexApiException(String message) {
        super(message);
    }

    public BittrexApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public BittrexApiException(Throwable cause) {
        super(cause);
    }

    public BittrexApiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
