package com.invest.crypto.bittrex.model.publicapi;

import java.math.BigDecimal;

import com.invest.crypto.bittrex.model.common.Response;

/**
 * @author Tabakov
 */
public class Ticker extends Response{

    private BigDecimal bid;
    private BigDecimal ask;
    private BigDecimal last;
    private long timeCalled;
    
    
    public BigDecimal getBid() {
        return bid;
    }

    public void setBid(BigDecimal bid) {
        this.bid = bid;
    }

    public BigDecimal getAsk() {
        return ask;
    }

    public void setAsk(BigDecimal ask) {
        this.ask = ask;
    }

    public BigDecimal getLast() {
        return last;
    }

    public void setLast(BigDecimal last) {
        this.last = last;
    }

	@Override
	public String toString() {
		return "Ticker [bid=" + bid + ", ask=" + ask + ", last=" + last + ", timeCalled=" + timeCalled + "]";
	}

	public long getTimeCalled() {
		return timeCalled;
	}

	public void setTimeCalled(long timeCalled) {
		this.timeCalled = timeCalled;
	}
    
}
