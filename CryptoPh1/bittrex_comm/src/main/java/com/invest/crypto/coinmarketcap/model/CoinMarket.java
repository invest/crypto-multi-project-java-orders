package com.invest.crypto.coinmarketcap.model;

import com.invest.crypto.bittrex.model.common.Response;

public final class CoinMarket extends Response {

	private String id;
	private String name;
	private String symbol;
	private int rank;
	private Double priceUSD;
	private Double priceBTC;
	private Double dailyVolumeUSD;
	private Double marketCapUSD;
	private Double availableSupply;
	private Double totalSupply;
	private Double maxSupply;
	private Double hourPrecentChange;
	private Double dayPrecentChange;
	private Double weekPercentChange;
	private long lastUpdated;
	private Double priceCurrencyRequested;
	private long timeCalled;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public Double getPriceUSD() {
		return priceUSD;
	}
	public void setPriceUSD(Double priceUSD) {
		this.priceUSD = priceUSD;
	}
	public Double getPriceBTC() {
		return priceBTC;
	}
	public void setPriceBTC(Double priceBTC) {
		this.priceBTC = priceBTC;
	}
	public Double getDailyVolumeUSD() {
		return dailyVolumeUSD;
	}
	public void setDailyVolumeUSD(Double dailyVolumeUSD) {
		this.dailyVolumeUSD = dailyVolumeUSD;
	}
	public Double getMarketCapUSD() {
		return marketCapUSD;
	}
	public void setMarketCapUSD(Double marketCapUSD) {
		this.marketCapUSD = marketCapUSD;
	}
	public Double getAvailableSupply() {
		return availableSupply;
	}
	public void setAvailableSupply(Double availableSupply) {
		this.availableSupply = availableSupply;
	}
	public Double getTotalSupply() {
		return totalSupply;
	}
	public void setTotalSupply(Double totalSupply) {
		this.totalSupply = totalSupply;
	}
	public Double getMaxSupply() {
		return maxSupply;
	}
	public void setMaxSupply(Double maxSupply) {
		this.maxSupply = maxSupply;
	}
	public Double getHourPrecentChange() {
		return hourPrecentChange;
	}
	public void setHourPrecentChange(Double hourPrecentChange) {
		this.hourPrecentChange = hourPrecentChange;
	}
	public Double getDayPrecentChange() {
		return dayPrecentChange;
	}
	public void setDayPrecentChange(Double dayPrecentChange) {
		this.dayPrecentChange = dayPrecentChange;
	}
	public Double getWeekPercentChange() {
		return weekPercentChange;
	}
	public void setWeekPercentChange(Double weekPercentChange) {
		this.weekPercentChange = weekPercentChange;
	}
	public long getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public Double getPriceCurrencyRequested() {
		return priceCurrencyRequested;
	}
	public void setPriceCurrencyRequested(Double priceCurrencyRequested) {
		this.priceCurrencyRequested = priceCurrencyRequested;
	}
	public long getTimeCalled() {
		return timeCalled;
	}
	public void setTimeCalled(long timeCalled) {
		this.timeCalled = timeCalled;
	}
	@Override
	public String toString() {
		return "CoinMarket [id=" + id + ", name=" + name + ", symbol=" + symbol + ", rank=" + rank + ", priceUSD="
				+ priceUSD + ", priceBTC=" + priceBTC + ", dailyVolumeUSD=" + dailyVolumeUSD + ", marketCapUSD="
				+ marketCapUSD + ", availableSupply=" + availableSupply + ", totalSupply=" + totalSupply
				+ ", maxSupply=" + maxSupply + ", hourPrecentChange=" + hourPrecentChange + ", dayPrecentChange="
				+ dayPrecentChange + ", weekPercentChange=" + weekPercentChange + ", lastUpdated=" + lastUpdated
				+ ", priceCurrencyRequested=" + priceCurrencyRequested + ", timeCalled=" + timeCalled + "]";
	}
}
