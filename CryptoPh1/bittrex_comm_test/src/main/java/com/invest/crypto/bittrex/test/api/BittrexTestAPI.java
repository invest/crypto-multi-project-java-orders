/**
 * 
 */
package com.invest.crypto.bittrex.test.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.invest.crypto.bittrex.test.config.ConfigManager;

/**
 * @author Tabakov
 *
 */
@Path("/test/api")
public class BittrexTestAPI {
	private static final Logger log = LogManager.getLogger(BittrexTestAPI.class);
	private static Map<String, LocalDateTime> marketHash = new HashMap<>();
	
	private static final String SUCCESS = "success";
	private static final String ORDER_DELAY = "orderdelay";
	private static final String MESSAGE = "message";
	private static final String AVAILABLE = "available";
	
	private static final String GETORDER = "getorder";
	private static final String BUYLIMIT = "buylimit";
	private static final String WITHDRAW = "withdraw";
	private static final String GETBALANCE = "getbalance";
	private static final String GET_WITHDRAWAL_HISTORY = "getwithdrawalhistory";
	
	private static final String baseUrl = "http://10.32.0.80:8080/bittrex_comm_test/test/api";
	
	@GET
	@Path("/market/buylimit")
	@Produces(MediaType.APPLICATION_JSON)
	public String buyLimit(@QueryParam("market") String market, @QueryParam("quantity") BigDecimal quantity, @QueryParam("rate") BigDecimal rate) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException {
		
		log.debug("buylimit is called with params -->  market: " + market + "quantity: " + quantity + "rate: " + rate);
		
		Map<String, String> serviceConfig = ConfigManager.getConfig(BUYLIMIT);
		String success = "false";
		String message = "";
		
		if (serviceConfig.isEmpty()) {
			log.debug("Unable to get configuration for service: buylimit");
			message = "Unable to get configuration for service: buylimit";
		}else {
			success = serviceConfig.get(SUCCESS);
			message = serviceConfig.get(MESSAGE);
		}
			
			String uuid = UUID.randomUUID().toString();
			log.debug("About to generate UUID.......");
			marketHash.put(uuid, LocalDateTime.now());
			
			String jsonResponse = "{" + 
					"\"success\":\""+success+"\"," + 
					"\"message\":\""+message+"\"," + 
					"\"result\":{" + 
					"\"uuid\":\""+uuid+"\"" + 
					"}" + 
					"}";

		return jsonResponse;
	}

	@GET
	@Path("/account/getorder")
	@Produces(MediaType.APPLICATION_JSON)
	public String getOrder(@QueryParam("uuid") String uuid) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException {
		log.debug("getorder is called with param: " + uuid);
		
		Map<String, String> serviceConfig = ConfigManager.getConfig(GETORDER);
		String success = "false";
		String message = "";
		String isOpen = "null";

		if (serviceConfig.isEmpty()) {
			log.debug("Unable to get configuration for service: getorder");
			message = "Unable to get configuration for service: getorder";
		} else {
			success = serviceConfig.get(SUCCESS);
			message = serviceConfig.get(MESSAGE);
			long delay = Long.valueOf(serviceConfig.get(ORDER_DELAY));
			if (marketHash.isEmpty()) {
				log.debug("There is no single deal inserted at bittrex exchange....");
				log.debug("Set configSuccess to FALSE....");
				success = "false";
			} else {
				if (marketHash.containsKey(uuid)) {
					if (marketHash.get(uuid).plusSeconds(delay).isBefore(LocalDateTime.now()) && success.equals("true")) {
						isOpen = "false";
						marketHash.remove(uuid);
					} else {
						isOpen = "true";
					}
				} else {
					log.debug("Bittrex exchange does not contain uuid: " + uuid);
					log.debug("Set configSuccess to FALSE && isOpen to FALSE....");
					isOpen = "false";
					success = "false";
				}
			}
		}
		
		String jsonResponse = "{" + 
				"\"success\":\""+success+"\"," + 
				"\"message\":\""+message+"\"," + 
				"\"result\":{" + 
				"\"AccountId\":null," + 
				"\"OrderUuid\":\""+uuid+"\"," + 
				"\"Exchange\" : \"BTC-SHLD\"," + 
				"\"Type\" : \"LIMIT_BUY\"," + 
				"\"Quantity\":1000.00000000," + 
				"\"QuantityRemaining\":1000.00000000," + 
				"\"Limit\":0.00000001," + 
				"\"Reserved\":0.00001000," + 
				"\"ReserveRemaining\":0.00001000," + 
				"\"CommissionReserved\":0.00000002," + 
				"\"CommissionReserveRemaining\":0.00000002," + 
				"\"CommissionPaid\":0.00000000," + 
				"\"Price\":0.00000000," + 
				"\"PricePerUnit\":null," + 
				"\"Opened\":\"2014-07-13T07:45:46.27\"," + 
				"\"Closed\":null," + 
				"\"IsOpen\":\""+isOpen+"\"," + 
				"\"Sentinel\":\"6c454604-22e2-4fb4-892e-179eede20972\"," + 
				"\"CancelInitiated\":false," + 
				"\"ImmediateOrCancel\":false," + 
				"\"IsConditional\" : false," + 
				"\"Condition\" : \"NONE\"," + 
				"\"ConditionTarget\" : null" + 
				"}" + 
				"}";
		
		return jsonResponse;
 }
	
	
	@GET
	@Path("/account/withdraw")
	@Produces(MediaType.APPLICATION_JSON)
	public String withdraw(@QueryParam("currency") String currency, @QueryParam("quantity") BigDecimal quantity, @QueryParam("address") String address) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException {
		
		log.debug("withdraw is called with params --> currency: " + currency + "quantity: " + quantity + "address: " + address);
		
		Map<String, String> serviceConfig = ConfigManager.getConfig(WITHDRAW);
		String success = "false";
		String message = "";
		
		if (serviceConfig.isEmpty()) {
			log.debug("Unable to get configuration for service: withdraw");
			message = "Unable to get configuration for service: withdraw";
		} else {
			success = serviceConfig.get(SUCCESS);
			message = serviceConfig.get(MESSAGE);
		}
		
			String jsonResponse = "{" + 
					"\"success\":\""+success+"\"," + 
					"\"message\":\""+message+"\"," + 
					"\"result\":{" + 
					"\"uuid\" : \"\"" + 
					"}" + 
					"}";
		
		return jsonResponse;
	}
	
	@GET
	@Path("/account/getbalance")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBalance(@QueryParam("currency") String currency) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException {
		
		log.debug("getBalance is called with params --> currency: " + currency);
		
		Map<String, String> serviceConfig = ConfigManager.getConfig(GETBALANCE);
		String success = "false";
		String message = "";
		String available = "";
		
		if (serviceConfig.isEmpty()) {
			log.debug("Unable to get configuration for service: getBalance");
			message = "Unable to get configuration for service: getBalance";
		} else {
			success = serviceConfig.get(SUCCESS);
			message = serviceConfig.get(MESSAGE);
			available = serviceConfig.get(AVAILABLE);
		}
		
			String jsonResponse = "{" + 
					"\"success\":\""+success+"\"," + 
					"\"message\":\""+message+"\"," + 
					"\"result\" : {" + 
					"\"Currency\" : \""+currency+"\"," + 
					"\"Balance\" : 4.21549076," + 
					"\"Available\" : "+available+"," + 
					"\"Pending\" : 0.00000000," + 
					"\"CryptoAddress\" : \"1MacMr6715hjds342dXuLqXcju6fgwHA31\"," + 
					"\"Requested\" : false," + 
					"\"Uuid\" : null" + 
					"}" + 
					"}";
		
		return jsonResponse;
	}
	
	@GET
	@Path("/account/getwithdrawalhistory")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWithdrawalHistory(@QueryParam("currency") String currency) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException {
		
		log.debug("getWithdrawalHistory is called with params --> currency: " + currency);
		
		Map<String, String> serviceConfig = ConfigManager.getConfig(GET_WITHDRAWAL_HISTORY);
		String success = "false";
		String message = "";
		
		if (serviceConfig.isEmpty()) {
			log.debug("Unable to get configuration for service: getWithdrawalHistory");
			message = "Unable to get configuration for service: getWithdrawalHistory";
		} else {
			success = serviceConfig.get(SUCCESS);
			message = serviceConfig.get(MESSAGE);
		}
		
			String jsonResponse = "{" + 
					"\"success\":\""+success+"\"," + 
					"\"message\":\""+message+"\"," + 
					"\"result\" : [{" + 
					"\"PaymentUuid\" : \"b52c7a5c-90c6-4c6e-835c-e16df12708b1\"," + 
					"\"Currency\" : \""+currency+"\"," +
					"\"Amount\" : 17.00000000," + 
					"\"Address\" : \"1DeaaFBdbB5nrHj87x3NHS4onvw1GPNyAu\"," + 
					"\"Opened\" : \"2014-07-09T04:24:47.217\"," + 
					"\"Authorized\" : true," + 
					"\"PendingPayment\" : false," + 
					"\"TxCost\" : 0.00020000," + 
					"\"TxId\" : null," + 
					"\"Canceled\" : true," + 
					"\"InvalidAddress\" : false" + 
					"}, {" + 
					"\"PaymentUuid\" : \"f293da98-788c-4188-a8f9-8ec2c33fdfcf\"," + 
					"\"Currency\" : \"XC\"," + 
					"\"Amount\" : 7513.75121715," + 
					"\"Address\" : \"XVnSMgAd7EonF2Dgc4c9K14L12RBaW5S5J\"," + 
					"\"Opened\" : \"2014-07-08T23:13:31.83\"," + 
					"\"Authorized\" : true," + 
					"\"PendingPayment\" : false," + 
					"\"TxCost\" : 0.00002000," + 
					"\"TxId\" : \"b4a575c2a71c7e56d02ab8e26bb1ef0a2f6cf2094f6ca2116476a569c1e84f6e\"," + 
					"\"Canceled\" : false," + 
					"\"InvalidAddress\" : false" + 
					"}" + 
					"]" + 
					"}";
		
		return jsonResponse;
	}
}
